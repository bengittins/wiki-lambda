import textwrap

import requests
import boto3
import json

from . import __version__

API_URL = "https://en.wikipedia.org/api/rest_v1/page/random/summary"


def main():
    """Wikipedia Lambda."""

    data = get_wiki_data(API_URL)

    title = data["title"]
    extract = data["extract"]
    
    print(title)
    print(textwrap.fill(extract))


def get_wiki_data(api):
    with requests.get(API_URL) as response:
        response.raise_for_status()
        data = response.json()

    return data
